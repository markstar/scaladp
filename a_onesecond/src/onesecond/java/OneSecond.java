package onesecond.java;

import java.util.*;

public class OneSecond {
    private final Vector<Integer> ints = new Vector<>();
    private int num = 0;

    public Integer getFirst() {
        return ints.firstElement();
    }

    public OneSecond(String str){
        ints.add(Integer.parseInt(str));
    }

    public OneSecond(int... elements) {
        for(int element: elements){
            ints.add(element);
        }
        System.out.println("Constructed.");
    }

    public Vector<Integer> getInts() {
        return ints;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
