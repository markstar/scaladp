package adapter.scala

trait IShape {
  def draw:Unit
}

class Rectangle extends IShape {
  def draw {
    println("Drawing rectangle")
  }
}

class TextDrawer(txt:String) {
  def drawText {
    println("Drawing "+txt)
  }
}

object TextDrawer {
  implicit def asIShape(src:TextDrawer) = new IShape {
    def draw {
      src.drawText
    }
  }
}