package decorator.java;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPInputStream;

public class Streams {
    public static void main(String[] args) throws IOException {
        DataInputStream is = new DataInputStream(
                (new FileInputStream("somefile.gz")));
        is.readInt();
        is.close();
    }
}
