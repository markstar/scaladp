package decorator.scala

trait InputStream {
  def core() {
    println("do core business.")
  }
}

trait FileInputStream extends InputStream {
  override def core() {
    println("fileInputstream behavior")
    super.core()
    println("additional fileInputStream behavior")
  }
}

trait GzipInputStream extends InputStream {
  override def core() {
    println("gzipInputstream behavior")
    super.core()
    println("additional gzipInputStream behavior")
  }
}

trait DataInputStream extends InputStream {
  override def core() {
    println("dataInputstream behavior")
    super.core()
    println("additional dataInputStream behavior")
  }
}

