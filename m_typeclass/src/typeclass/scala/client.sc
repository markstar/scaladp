import typeclass.scala.Reversible

def verifyReversible[T : Reversible](v: T) {
  val ev = implicitly[Reversible[T]]
  assert(ev.rev(ev.rev(v)) == v)
}

import typeclass.scala.Reversibles._
//verifyReversible("abcdefg")
//verifyReversible(123)
"abcdefg".rev
123.rev
